package model

import (
	"bytes"
	"fmt"
	"strings"
)

type IdentityInterface interface {
	Encode() []byte
	Decode([]byte)
}
type SUCI struct {
	supiFormat             string
	mcc                    string
	mnc                    string
	routing                string
	schemeOut              string
	protectSchemeID        string
	homeNetworkPublicKeyID string
	buffer                 []byte
}

func NewSUCI(identity []byte) *SUCI {

	s := SUCI{}
	supiFormat := identity[0] >> 4 & 0x07
	mcc1 := identity[1] & 0x0f
	mcc2 := identity[1] >> 4 & 0x0f
	mcc3 := identity[2] & 0x0f
	mnc3 := identity[2] >> 4 & 0x0f
	mnc1 := identity[3] & 0x0f
	mnc2 := identity[3] >> 4 & 0x0f
	routingInd1 := identity[4] & 0x0f
	routingInd2 := identity[4] >> 4 & 0x0f
	routingInd3 := identity[5] & 0x0f
	routingInd4 := identity[5] >> 4 & 0x0f
	protectSchemeId := identity[6] & 0x0f
	homeNPID := identity[7]

	remain := identity[8:]
	var schemeOut []byte
	for _, b := range remain {
		schemeOut = append(schemeOut, b&0x0f)

		l := b >> 4 & 0x0f
		if l == 0x0f {
			break
		}
		schemeOut = append(schemeOut, b>>4&0x0f)
	}
	s.supiFormat = WriteCharacter(supiFormat)
	s.mcc = WriteCharacter(mcc1, mcc2, mcc3)
	s.mnc = WriteCharacter(mnc1, mnc2, mnc3)
	s.routing = WriteCharacter(routingInd1, routingInd2, routingInd3, routingInd4)
	s.schemeOut = WriteCharacter(schemeOut...)
	s.protectSchemeID = WriteCharacter(protectSchemeId)
	s.homeNetworkPublicKeyID = WriteCharacter(homeNPID)
	fmt.Printf("%#v\n", s)

	return &s
}

func (s *SUCI) String() string {
	return strings.Join([]string{"suci", s.supiFormat, s.mcc, s.mnc, s.routing, s.protectSchemeID, s.homeNetworkPublicKeyID, s.schemeOut}, "-")
}

func (s *SUCI) Bytes() []byte {
	var buf bytes.Buffer
	b1 := ReadCharacter(s.supiFormat)
	b2 := ReadCharacter(s.mcc)
	b3 := ReadCharacter(s.mnc)
	buf.WriteByte(b1[0])
	buf.WriteByte(b2[1]<<4 | b2[0])
	buf.WriteByte(b3[2]<<4 | b2[2])
	buf.WriteByte(b3[1]<<4 | b3[0])
	b4 := ReadCharacter(s.routing)
	for i := 0; i < len(b4); i += 2 {
		buf.WriteByte(b4[i+1]<<4 | b4[i])
	}
	b5 := ReadCharacter(s.protectSchemeID)
	buf.WriteByte(b5[0])
	b6 := ReadCharacter(s.homeNetworkPublicKeyID)
	buf.WriteByte(b6[0])
	b7 := ReadCharacter(s.schemeOut)
	for i := 0; i < len(b7); i += 2 {
		if i+1 == len(b7) {
			buf.WriteByte(0x0f<<4 | b7[i])
		} else {
			buf.WriteByte(b7[i+1]<<4 | b7[i])
		}

	}
	return buf.Bytes()
}
func WriteCharacter(b ...byte) string {
	var buf strings.Builder
	for _, v := range b {
		buf.WriteByte(v + 48) // "0" is 48 in ascii code
	}
	return buf.String()
}

func ReadCharacter(s string) []byte {
	var buf bytes.Buffer
	r := []rune(s)
	for _, i := range r {
		buf.WriteRune(i - 48)
	}
	return buf.Bytes()
}
