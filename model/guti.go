package model

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
)

type GUTI5G struct {
	typeIdentity []byte
	mcc          []byte
	mnc          []byte
	amfRegionID  []byte
	amfSetID     []byte
	amfPointer   []byte
	tmsi         []byte
}

func NewGUTI(buf []byte) *GUTI5G {
	s := GUTI5G{
		typeIdentity: make([]byte, 1),
		mcc:          make([]byte, 3),
		mnc:          make([]byte, 3),
		amfRegionID:  make([]byte, 1),
		amfSetID:     make([]byte, 2),
		amfPointer:   make([]byte, 1),
		tmsi:         make([]byte, 4),
	}
	s.typeIdentity[0] = buf[0]
	s.mcc[0] = buf[1] & 0x0f
	s.mcc[1] = buf[1] >> 4 & 0x0f
	s.mcc[2] = buf[2] & 0x0f
	s.mnc[2] = buf[2] >> 4 & 0x0f
	s.mnc[0] = buf[3] & 0x0f
	s.mnc[1] = buf[3] >> 4 & 0x0f
	s.amfRegionID[0] = buf[4]
	s.amfSetID[0] = buf[5]
	s.amfSetID[1] = buf[6] >> 6 & 0x0f
	s.amfPointer[0] = buf[6] & 0x3f
	s.tmsi = buf[7:]

	return &s

}

func (s *GUTI5G) String() string {

	mcc := WriteCharacter(s.mcc...)
	mnc := WriteCharacter(s.mnc...)
	regionID := strconv.FormatUint(uint64(s.amfRegionID[0]), 10)
	amfSetID := s.amfSetID[0]<<2 | s.amfSetID[1]
	setStr := strconv.FormatUint(uint64(amfSetID), 10)
	pointerID := strconv.FormatUint(uint64(s.amfPointer[0]), 10)
	var buf strings.Builder
	for _, i := range s.tmsi {
		buf.WriteString(fmt.Sprintf("%x", i))
	}
	tmsi := buf.String()
	tmsi = hex.EncodeToString(s.tmsi)
	return strings.Join([]string{mcc, mnc, regionID, setStr, pointerID, tmsi}, "-")
}

func (s *GUTI5G) Bytes() []byte {
	var buf bytes.Buffer
	buf.Write(s.typeIdentity)
	buf.WriteByte(s.mcc[1]<<4 | s.mcc[0])
	buf.WriteByte(s.mnc[2]<<4 | s.mcc[2])
	buf.WriteByte(s.mnc[1]<<4 | s.mnc[0])
	buf.Write(s.amfRegionID)
	buf.WriteByte(s.amfSetID[0])
	buf.WriteByte(s.amfSetID[1]<<6 | s.amfPointer[0])
	buf.Write(s.tmsi)

	return buf.Bytes()
}
