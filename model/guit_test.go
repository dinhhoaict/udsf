package model_test

import (
	"encoding/hex"
	"fmt"
	"testing"
	"udsf/model"

	"github.com/ishidawataru/sctp"
)

func TestGUTI(t *testing.T) {
	s := "f264001062190a01002800"
	b, _ := hex.DecodeString(s)
	fmt.Printf("%08b\n", b)
	guti := model.NewGUTI(b)
	fmt.Printf("%#v\n", guti)
	str := guti.String()
	fmt.Println(str)
	retByte := guti.Bytes()
	fmt.Printf("%08b\n", retByte)
	a := sctp.SCTPAddr{}
	fmt.Println(a)
}
