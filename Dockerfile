FROM golang

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64


WORKDIR /app
COPY . /app
# RUN go mod download
RUN go build -mod=vendor -o udsf .

# final stage
EXPOSE 8080
ENTRYPOINT ["/app/udsf"]

